﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootingEnemy : Enemy
{

    public Rigidbody body;
    public ParticleSystem smoke;

    float originalHeight;
    // Use this for initialization
    public override void Start()
    {
        type = "Shooting";
        fundsWorth = 100;
        currentHealth = maxHealth;
        originalHeight = this.transform.position.y;

        base.Start();
    }


}
