﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class EnemyHitScanSystem : NetworkBehaviour
{
	public ParticleSystem muzzleFlash;

	public float attackRate;
	public float damage;
	public float dps;
    public float fireTimer;
	float attacksPerSecond;

	public AudioSource shootSound;
	public AudioClip shoot;
	public AudioClip windup;
	private bool hasPlayed = false;

	public GameObject target;
	public float fieldOfView;
    Enemy enemy;
    GameObject playerBase;
	// Use this for initialization
	void Start() {
        enemy = GetComponent<Enemy>();
        damage = enemy.weaponDamage;
        playerBase = GameObject.FindGameObjectWithTag("Base");
	}

	// Update is called once per frame
	void Update()
	{
        if (playerBase == null) {
            playerBase = GameObject.FindGameObjectWithTag("Base");
        }
        if (!isServer) return;
        enemy = GetComponent<Enemy>();
        damage = enemy.weaponDamage;
        attacksPerSecond = 1 / attackRate;
		dps = (float)damage * attacksPerSecond;
        if (enemy.target == Enemy.Target.PlayerBase) {
            target = playerBase;
        } else if (enemy.target == Enemy.Target.Player) {
            target = GameManager.GetClosestPlayer(enemy.targetPos).gameObject;
        } else {
            target = null;
        }
  //      if (gameObject.GetComponent<PlayerTracking>().target == null) return;
		//target = gameObject.GetComponent<PlayerTracking>().target;
        fireTimer += Time.deltaTime;
        if (target != null)
		{
			if (!shootSound.isPlaying && !hasPlayed) {
					shootSound.PlayOneShot (windup);
					hasPlayed = true;
					muzzleFlash.Stop ();
				}

				float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                //float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                float distance = Vector3.Distance(transform.position, target.transform.position);
                if (angle < fieldOfView && distance <= GetComponent<Enemy>().weaponRange && fireTimer >= attackRate && target.GetComponent<BaseHealth>())
				{
					
                    fireTimer = 0;
					Debug.Log ("Enemy tracking: " + target.name + " target HP: " + target.gameObject.GetComponent<BaseHealth> ().currentHealth);
                    Debug.Log("Enemy HP: " +GetComponent<BaseHealth>().currentHealth);

					shootSound.PlayOneShot (shoot);
					hasPlayed = false;
                    muzzleFlash.Play ();
                    //this.gameObject.GetComponent<AudioSource> ().Play ();
                    target.gameObject.GetComponent<BaseHealth>().TakeDamage(damage);
			

				}
            }
		}
	}
