﻿using UnityEngine;
using System.Collections;

public class MovieTextureScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
		((MovieTexture)GetComponent<Renderer> ().material.mainTexture).loop = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!(((MovieTexture)GetComponent<Renderer> ().material.mainTexture).isPlaying)) {
			((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
			((MovieTexture)GetComponent<Renderer> ().material.mainTexture).loop = true;
		}
	}
}
